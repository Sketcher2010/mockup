﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public float moveSmoothing = 5f;
	public float rotateSmoothing = 1f;
	private Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = this.transform.position - target.position;
	}
	

	void FixedUpdate () {
		float xAngle = this.transform.rotation.x;
		Quaternion rotateTo = target.rotation;
		rotateTo.x = xAngle;
		this.transform.rotation = Quaternion.Lerp (this.transform.rotation, rotateTo, rotateSmoothing * Time.deltaTime);

		//Vector3 cameraMovePosition = target.position + offset;
		//this.transform.position = Vector3.Lerp (this.transform.position, cameraMovePosition, moveSmoothing * Time.deltaTime);

	}
}
