﻿using UnityEngine;
using System.Collections;

public class Weapon_RayCast3rd : MonoBehaviour {

	public int damage = 0;
	public Transform effect;
	public float fireRate = 0.2f;
	public float range = 5f;
	public Animator animator;
	private float cooldown = 0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		cooldown -= Time.deltaTime;

		if (cooldown > 0)
			return;

		if (Input.GetMouseButtonDown (0)) {

			if (gameObject.GetComponent<AudioSource>() != null){
				gameObject.GetComponent<AudioSource>().Play();
			}

			RaycastHit hit;

			Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast (mouseRay, out hit, 100)) {

				Ray ray = new Ray(this.transform.position, hit.transform.position);

				/*
				LineRenderer line = this.GetComponent<LineRenderer>();
				line.SetPosition(0, this.transform.position);
				line.SetPosition(1, hit.point);
				*/

				if (Physics.Raycast (ray, out hit, range)) {

					hit.collider.SendMessage ("applyDamage", damage, SendMessageOptions.DontRequireReceiver);

					Debug.Log ("hit the " + hit.collider.name + " " + hit.point);

				}

			}

		}

	}
}
