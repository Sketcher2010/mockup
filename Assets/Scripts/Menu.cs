﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	private int window = 0;

	public string ip = "localhost";
	public string port = "25000";
	public int maxConnections = 20;
	public GUISkin skin;

	/*
		windows:
			0 = start menu;
			1 = server info
			2 = client room
	 */

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnServerInitialized() {
		window = 1;
	}

	void OnConnectedToServer() {
		window = 3;
	}

	void DisconnectServer() {
		Network.Disconnect ();
		window = 0;
	}

	void OnGUI() {
		GUI.skin = skin;
		if (window == 0) {
			if (GUI.Button (new Rect (Screen.width / 2 - 100, 155, 200, 40), "Host server")) {
				Network.InitializeServer(maxConnections, int.Parse(port), true);
			}
			if (GUI.Button (new Rect (Screen.width / 2 - 100, 205, 200, 40), "Connect to server")) {
				window = 2;
			}
			if(GUI.Button (new Rect (Screen.width / 2 - 100, 255, 200, 40), "Quit")) {
				Application.Quit();
			}
		}
		if (window == 1) {
			GUI.Label (new Rect (Screen.width / 2 - 100, 255, 200, 40), "Connections: "+Network.connections.Length);
			if (GUI.Button (new Rect (Screen.width / 2 - 100, 155, 200, 40), "Shut Down")) {
				DisconnectServer();
			}
			if(GUI.Button (new Rect (Screen.width / 2 - 100, 205, 200, 40), "PLAY GAME!")) {
				GetComponent<NetworkView>().RPC("loadLevel", RPCMode.All);
			}
		}
		if(window == 2) {
			ip = GUI.TextField(new Rect (Screen.width / 2 - 100, 155, 200, 40), ip, 15);
			if(GUI.Button (new Rect (Screen.width / 2 - 100, 205, 200, 40), "Connect")) {
				window = 4;
				try  {
					Network.Connect(ip, int.Parse(port));
				} catch {
					window = 2;
					string errorMessage = "Could not connect to server: ";
					GUI.Label(new Rect (Screen.width / 2 - 100, 305, 200, 40), errorMessage);
				}
			}
			if(GUI.Button (new Rect (Screen.width / 2 - 100, 255, 200, 40), "Back")) {
				window = 0;
			}
		}
		if (window == 3) {
			if (GUI.Button (new Rect (Screen.width / 2 - 100, 155, 200, 40), "Disconnect")) {
				DisconnectServer();
				Network.Disconnect();
			}
		}
		if (window == 4) {
			GUI.Label(new Rect (Screen.width / 2 - 100, 155, 200, 40), "Connecting...");
		}
	}
	[RPC]
	void loadLevel() {
		Application.LoadLevel(1);
	}
}
