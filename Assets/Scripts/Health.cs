﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public int Health_Max = 100;
	public int Health_Cur = 100;
	public GUISkin skin;
	private int hpl = Screen.width-125;
	public int hpt;
	public int hpw;
	public int hph;
	
	void OnGUI() {
		GUI.skin = skin;

		float HealthBarL = (float) Health_Cur / Health_Max;

		GUI.Label (new Rect(hpl, hpt, hpw, hph), "HP: "+Health_Cur+"%", GUI.skin.GetStyle("Health_text"));
		GUI.Box (new Rect(Screen.width-220, 20, 200, 25), " ", GUI.skin.GetStyle("Health_bg"));
		GUI.Box (new Rect(Screen.width-220, 20, 200*HealthBarL, 25), " ", GUI.skin.GetStyle("Health_neon"));
	}
}
