﻿using UnityEngine;
using System.Collections;

public class HealthScript : MonoBehaviour {
	public int health = 100;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void applyDamage(int damage){
		health -= damage;

		if (health <= 0) {
			if (this.tag == "Player"){
				this.transform.position = new Vector3(0, 11, 0);
				this.GetComponent<HealthScript>().health = 100;
			}else{
				Network.Destroy(this.gameObject);
			}
		}
	}
}
