﻿using UnityEngine;
using System.Collections;

public class SmoothMovement : MonoBehaviour {

	private Vector3 lastPos;
	private Quaternion lastAngle;
	private Transform myTransform;
	private NetworkView nView;

	// Use this for initialization
	void Start () {
		nView = this.GetComponent <NetworkView>();

		if (nView.isMine)
			myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance (myTransform.position, lastPos) >= 0.05f || Quaternion.Angle (myTransform.rotation, lastAngle) >= 1) {
			lastPos = myTransform.position;
			lastAngle = myTransform.rotation;
			nView.RPC("UpdateMovement", RPCMode.OthersBuffered, lastPos, lastAngle);
		}
	}

	[RPC]

	void UpdateMovement(Vector3 newPos, Quaternion newAngle){
		transform.position = newPos;
		transform.rotation = newAngle;
	}
}
