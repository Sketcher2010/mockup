﻿using UnityEngine;
using System.Collections;

namespace GameSpace{

	public class TimedDestroyer : MonoBehaviour {
		private float timer = 0;
		private GameObject destroyingObject;
		private float delay = 1f;

		public TimedDestroyer(GameObject destroyingObject, float delay){
			this.destroyingObject = destroyingObject;
			this.delay = delay;
		}

		void Awake(){
			timer = Time.time;
		}
		
		// Update is called once per frame
		void Update () {
			if (Time.time - timer < delay)
				return;

			Network.Destroy (destroyingObject);

			Destroy (this);
		}
	}

}
