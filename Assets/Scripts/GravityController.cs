﻿using UnityEngine;
using System.Collections;

public class GravityController : MonoBehaviour {

	public float rotateSpeed = 10f;

	public bool rotated = false;
	private Camera camera;
	private Quaternion fromAngle;
	private Quaternion rotateTo;
	private bool isRotating = false;

	// Use this for initialization
	void Start () {
		foreach (Camera camera in GetComponentsInChildren<Camera>())
			if (camera.gameObject.name == "Camera")
				this.camera = camera;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.V) && !isRotating) {
			//fromAngle = transform.rotation;
			//rotateTo = transform.rotation;
			//rotateTo *= Quaternion.AngleAxis(180, Vector3.forward);
			rotated = !rotated;
			isRotating = true;
			//transform.rotation = Quaternion.Lerp(transform.rotation, rotateTo, Time.time);
		}

		if (isRotating && !GetComponent<FPSController>().getGrounded() &&
		    ( 
		 	(transform.rotation.eulerAngles.z < 180f && transform.rotation.eulerAngles.z >= 0f && rotated)
		 	||
		 	(transform.rotation.eulerAngles.z < 360f && transform.rotation.eulerAngles.z >= 180f && !rotated)
		 	)
		    ) {
			//transform.rotation = Quaternion.Slerp (transform.rotation, rotateTo, Time.deltaTime * rotateSpeed);
			transform.Rotate (Vector3.forward * (rotateSpeed * Time.deltaTime));
		} else if (isRotating)
			isRotating = false;

		/*
		if (isRotating){ //&& !transform.rotation.Equals (rotateTo)) {
			Debug.Log (transform.rotation);
			//transform.rotation = Quaternion.Slerp(transform.rotation, rotateTo, Time.deltaTime * 0.00001f);
			//transform.Rotate (Vector3.forward * (rotateSpeed * Time.deltaTime));

		}else if (isRotating)
			isRotating = false;
		*/
	}

	public bool getParamRotated(){
		return rotated;
	}

	public bool getParamIsRotating(){
		return isRotating;
	}

	public float getParamRotateValue(){
		return (rotated) ? -1f : 1f;
	}
}
