﻿using UnityEngine;
using System.Collections;

public class CrosshairScript3rd : MonoBehaviour {
	public Texture2D crosshairTexture; 
	private Rect rect; 
	static bool showCrosshair = true;

	// Use this for initialization
	void Start () {
		rect = new Rect((Screen.width - crosshairTexture.width) / 2, (Screen.height - crosshairTexture.height) /2, crosshairTexture.width, crosshairTexture.height);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rect.position = Input.mousePosition - new Vector3 (crosshairTexture.width / 2, 0, 0);
	}

	void OnGUI () {
		if (showCrosshair == true) {
			//GUI.DrawTexture (rect, crosshairTexture);
		}
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;
		Cursor.SetCursor (crosshairTexture, new Vector2(crosshairTexture.width / 2, crosshairTexture.height / 2), CursorMode.Auto);
	}
}
