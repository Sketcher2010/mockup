﻿using UnityEngine;
using System.Collections;
using GameSpace;

public class Weapon_RayCastFPS : MonoBehaviour {

	public int damage = 10;
	public Transform effect;
	public float fireRate = 0.2f;
	public float range = 100f;
	public Animator animator;
	private float cooldown = 0f;
	private NetworkView nView;
	private Camera camera;

	// Use this for initialization
	void Start () {
		nView = GetComponent<NetworkView>();

		foreach (Camera camera in GetComponentsInParent<Camera>())
			if (camera.gameObject.name == "Camera")
				this.camera = camera;
	}
	
	// Update is called once per frame
	void Update () {
		if (!nView.isMine)
			return;

		cooldown -= Time.deltaTime;
		
		if (cooldown > 0)
			return;

		if (Input.GetMouseButtonDown (0)) {

			if (gameObject.GetComponent<AudioSource>() != null){
				gameObject.GetComponent<AudioSource>().Play ();
			}

			animator.SetTrigger("Shoot");

			RaycastHit hit;

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			
			if (Physics.Raycast (ray, out hit, range)) {
				/*
				LineRenderer line = this.GetComponent<LineRenderer>();
				line.SetPosition(0, this.transform.position);
				line.SetPosition(1, hit.point);
				*/

				Transform effectClone = Network.Instantiate(effect, hit.point, Quaternion.identity, 0) as Transform;

				StartCoroutine(delayedDelete(effectClone.gameObject, 1f));

				//new TimedDestroyer(effectClone.gameObject, 1f);

				hit.collider.SendMessage ("applyDamage", damage, SendMessageOptions.DontRequireReceiver);
				
			}

		}

	}

	IEnumerator delayedDelete(GameObject deletingObject, float delay){
		yield return new WaitForSeconds(delay);

		Network.Destroy(deletingObject);
	}
}
