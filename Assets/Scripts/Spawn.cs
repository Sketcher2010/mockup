﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

	public Transform spawnObject;
	public int spawnCount = 20;

	// Use this for initialization
	void Start () {
		while (spawnCount-- > 0) {
			Vector3 spawnPosition = transform.position + new Vector3(Random.Range(-25, 25), 0, Random.Range(-25, 25));
			Transform gameObject = (Transform)Network.Instantiate(spawnObject, spawnPosition, Quaternion.identity, 0);
			gameObject.parent = GameObject.FindGameObjectWithTag("Finish").transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
