﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {

	public Transform playerObject;

	// Use this for initialization
	void Start () {
		Transform player = (Transform)Network.Instantiate (playerObject, transform.position + new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10)), Quaternion.identity, 0);
		player.parent = GameObject.FindGameObjectWithTag ("Finish").transform;
		foreach(Camera camera in player.GetComponentsInChildren<Camera>()){
			camera.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
