﻿using UnityEngine;
using System.Collections;

public class FPSCharacterController : MonoBehaviour {

	public float walkSpeed = 6f;
	public float mouseSensivity = 2f;
	public float jumpSpeed = 1f;
	public bool toggleRun = false;
	public float fallingDamageThreshold = 10.0f;
	public bool airControl = false;
	
	public float minimumX = -90f;
	public float maximumX = 90f;
	public float minimumY = -360f;
	public float maximumY = 360f;
	
	public bool gravityControl = true;
	public float rotateSpeed = 10f;
	
	private bool rotated = false;
	private Quaternion fromAngle;
	private Quaternion rotateTo;
	private bool isRotating = false;

	
	private Vector3 moveDirection = Vector3.zero;
	private string mouseAxisNameX = "Mouse X";
	private string mouseAxisNameY = "Mouse Y";
	private float inputX;
	private float inputY;
	private CharacterController controller;
	private Transform myTransform;
	private Camera camera;
	private float rotateX = 0f;
	private float rotateY = 0f;
	private bool grounded = false;
	private bool falling = false;
	private bool playerControl = false;
	private float fallStartLevel;
	private float speed;
	private bool headGrounded = false;

	// Use this for initialization
	void Start () {
		myTransform = transform;
		controller = GetComponent<CharacterController>();
		speed = walkSpeed;
		
		foreach (Camera camera in GetComponentsInChildren<Camera>())
			if (camera.gameObject.name == "Camera")
				this.camera = camera;
	}
	
	// Update is called once per frame
	void Update () {
		gravityUpdate ();
		Turn ();
	}

	void FixedUpdate () {
		getInput ();
		
		float inputModifyFactor = (inputX != 0.0f && inputY != 0.0f) ? .7071f : 1.0f;
		
		if (grounded) {
			
			if (falling) {
				falling = false;
				if (myTransform.position.y < fallStartLevel - fallingDamageThreshold)
					FallingDamage (fallStartLevel - myTransform.position.y);
			}
			
			moveDirection = new Vector3(inputX * inputModifyFactor, 0, inputY * inputModifyFactor);
			moveDirection = myTransform.TransformDirection(moveDirection) * speed;
			playerControl = true;
			
			if (Input.GetButton("Jump"))
				moveDirection.y = jumpSpeed;
			
		} else {
			
			if (!falling) {
				falling = true;
				fallStartLevel = myTransform.position.y;
			}
			
			if (airControl && playerControl) {
				moveDirection.x = inputX * speed * inputModifyFactor;
				moveDirection.z = inputY * speed * inputModifyFactor;
				moveDirection = myTransform.TransformDirection(moveDirection);
			}

			Vector3 g = Physics.gravity * Time.deltaTime;// * ((GetComponent<GravityController>().getParamRotated()) ? -1 : 1 );
			moveDirection.Set (moveDirection.x + g.x, moveDirection.y + g.y , moveDirection.z + g.z);
		}

		//moveDirection = myTransform.TransformDirection(moveDirection);

		Debug.Log (Time.time);
		
		CollisionFlags flags = controller.Move (moveDirection * Time.deltaTime);
		grounded = (flags & CollisionFlags.Below ) != 0;
		headGrounded = (flags & CollisionFlags.Above ) != 0;

		if (headGrounded) {
			moveDirection.y = 0;
			Debug.Log("Head Grounded. " + Time.time);
		}

		if (grounded) {
			moveDirection.y = 0;
			Debug.Log("Grounded. " + Time.time);
		}
		
		if (Input.GetKey(KeyCode.E)) {
			Debug.Log (grounded + ". " + Time.time);
		}
	}
	
	void gravityUpdate (){
		if (Input.GetKeyDown (KeyCode.G)) {
			//Physics.gravity = Physics.gravity * -1;
			Quaternion QuatOfRotObj = GameObject.FindGameObjectWithTag("Finish").transform.rotation;
			QuatOfRotObj.eulerAngles.Set(0, 0, ((QuatOfRotObj.eulerAngles.z == 0) ? 180 : 0));
		}
		if (Input.GetKeyDown (KeyCode.V) && !isRotating) {
			//rotated = !rotated;
			//isRotating = true;
		}
	}

	void FallingDamage (float fallDistance) {
		print ("Ouch! Fell " + fallDistance + " units!");
	}
	
	void Turn(){
		rotateX += Input.GetAxis("Mouse X") * mouseSensivity * ((rotated) ? -1f : 1f);
		rotateY += Input.GetAxis("Mouse Y") * mouseSensivity;
		
		//Debug.Log (rotateX + " " + rotateY);
		//Debug.Log (GetComponent<GravityController>().getParamRotated());
		
		Adjust360andClamp ();
		
		if (!rotated| grounded) {
			transform.localRotation = Quaternion.AngleAxis (rotateX, Vector3.up);
			rotated = false;
		} else {
			transform.localRotation = Quaternion.AngleAxis (180, Vector3.forward);
			transform.localRotation *= Quaternion.AngleAxis (rotateX, Vector3.down);
		}
		
		camera.transform.localRotation = Quaternion.AngleAxis (rotateY, Vector3.left);
	}
	
	void getInput(){
		inputX = Input.GetAxis("Horizontal");
		inputY = Input.GetAxis("Vertical");
	}
	
	void Adjust360andClamp (){
		if (rotateX < -360)
		{
			rotateX += 360;
		}
		else if (rotateX > 360)
		{
			rotateX -= 360;
		}   
		
		// Don't let our Y go beyond 360 degrees + or -
		if (rotateY < -360)
		{
			rotateY += 360;
		}
		else if (rotateY > 360)
		{
			rotateY -= 360;
		}
		
		// Clamp our angles to the min and max set in the Inspector
		//rotateX = Mathf.Clamp (rotateX, minimumX, maximumX);
		rotateY = Mathf.Clamp (rotateY, minimumY, maximumY);
	}
}
